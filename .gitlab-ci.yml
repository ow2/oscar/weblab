stages:
  - clone
  - scan
  - test
  - sonar

variables:
  ## Per project variable

  # URL of the server hosting the project
  GIT_SERVER_URL: 'https://gitlab.ow2.org'
  # Name of the organization (GitHub) or group (GitLab)
  GIT_GROUP_NAME: 'weblab'
  # Name of the project on GitHub or GitLab (might be the same as GIT_GROUP_NAME)
  GIT_PROJECT_NAME: 'core'
  # Name of the project as defined by OW2 (might be the same as GIT_GROUP_NAME and/or GIT_GROUP_NAME)
  OW2_PROJECT_NAME: 'weblab'
  # Branch to scan, build and test (usually master or main)
  GIT_BRANCH_NAME: 'master'

  ## "Constants"

  # Location to store the clone of all projects processed by MRL
  # This folder is mounted in Docker image executed by GitLab
  PROJECT_SOURCES_BASEDIR: '/srv/project-sources'
  # Relative (to PROJECT_SOURCES_BASEDIR) path to which a project Git repository will be checkout
  PROJECT_CHECKOUT_DIR_RELATIVE_PATH: '$GIT_GROUP_NAME/$GIT_PROJECT_NAME'
  # Folder where the project Git repository is cloned
  # Can't reuse PROJECT_CHECKOUT_DIR_RELATIVE_PATH due to https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1809
  PROJECT_SOURCES_DIR: '$PROJECT_SOURCES_BASEDIR/$GIT_GROUP_NAME/$GIT_PROJECT_NAME'
  # Project full Git repository URL
  PROJECT_GIT_URL: '$GIT_SERVER_URL/$GIT_GROUP_NAME/$GIT_PROJECT_NAME.git'

  ## Scancode
  ### Variables
  SCANCODE_OPTIONS_PROJECT_SPECIFIC: ''

  ### Constants
  SCANCODE_OPTIONS: '-n 4 --timeout 7200 -l $SCANCODE_OPTIONS_PROJECT_SPECIFIC'
  SCANCODE_FILENAME: 'scancode-$OW2_PROJECT_NAME.json'

  ## Java
  ### Variables
  TEST_JAVA_VERSION: '8'
  SONARQUBE_JAVA_VERSION: '11'

  ## Maven
  ### Variables
  # Maven version
  MAVEN_VERSION: '3'
  # Project specific Maven CLI options
  MAVEN_CLI_OPTS_PROJECT_SPECIFIC: ''
  # Project specific Maven options
  MAVEN_OPTS_PROJECT_SPECIFIC: ''

  ### Constants
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  # MAVEN_OPTS is automatically sourced by Maven (no need to add it on the command line)
  MAVEN_OPTS: '-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true $MAVEN_OPTS_PROJECT_SPECIFIC'
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: '--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true $MAVEN_CLI_OPTS_PROJECT_SPECIFIC'

  ## SonarQube
  ### Constants
  SONAR_URL: 'https://sonarqube.ow2.org'

cache:
  paths:
    # Maven
    - .m2/repository

clonerepo:
  image: maven:latest
  stage: clone
  script:
    - '[ -d $PROJECT_SOURCES_DIR ] && (cd $PROJECT_SOURCES_DIR && git fetch) || (cd $PROJECT_SOURCES_BASEDIR && git clone $PROJECT_GIT_URL $PROJECT_CHECKOUT_DIR_RELATIVE_PATH)'
    - 'cd $PROJECT_SOURCES_DIR && git clean -dfxq && git reset --hard origin/$GIT_BRANCH_NAME'

scancode:
  tags:
    - scancode
  stage: scan
  script:
    - '$HOME/scancode $SCANCODE_OPTIONS --json=$SCANCODE_FILENAME $PROJECT_SOURCES_DIR'
    - echo "If scancode ran properly, the output file is available at ${CI_PROJECT_URL}/-/jobs/artifacts/main/raw/${SCANCODE_FILENAME}?job=${CI_JOB_NAME}"
  artifacts:
    paths:
      # will produce a json file available at:
      # https://gitlab.ow2.org/ow2/oscar/$OW2_PROJECT_NAME/-/jobs/artifacts/main/raw/$SCANCODE_FILENAME?job=scancode
      - $SCANCODE_FILENAME

test:
  image: 'maven:$MAVEN_VERSION-eclipse-temurin-$TEST_JAVA_VERSION'
  stage: test
  script:
    - 'cd $PROJECT_SOURCES_DIR'
    - 'mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test org.jacoco:jacoco-maven-plugin:report'

sonar:
  image: 'maven:$MAVEN_VERSION-eclipse-temurin-$SONARQUBE_JAVA_VERSION'
  stage: sonar
  script:
    - 'cd $PROJECT_SOURCES_DIR'
    - 'mvn $MAVEN_CLI_OPTS -Dsonar.host.url=${SONAR_URL} -Dsonar.login=${SONAR_LOGIN} sonar:sonar'
